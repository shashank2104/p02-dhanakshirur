//
//  ViewController.m
//  p02-dhanakshirur
//
//  Created by Shashank Dhanakshirur on 2/9/16.
//  Copyright © 2016 sdhanak1. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize cellData;
@synthesize score;
@synthesize gameWonView;
static int rand1;
static int rand2;
static int flag;
static int gameWonFlag;
static int gameOverFlag;
static int stillEqualCells;

- (void)viewDidLoad {
    [super viewDidLoad];

    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background"]];
    [self.view addSubview:(backgroundImage)];
    UIImageView *gameWonImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"GameWon"]];
    [gameWonView addSubview:(gameWonImage)];

    gameWonView.hidden = YES;
    //self.view.hidden = NO;

    int r1 = arc4random_uniform(16);
    int r2 = arc4random_uniform(16);
    while(r2 == r1)
    {
        r2 = arc4random_uniform(16);
    }
    rand1 = r1;
    rand2 = r2;

    UILabel *label1 = [cellData objectAtIndex:rand1];
    [label1 setText:@"2"];
    UILabel *label2 = [cellData objectAtIndex:rand2];
    [label2 setText:@"2"];
    
    [score setText:@"0"];

    // Swipe gestures
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(updatesOnRightSwipe:)];
      swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
        [self.view addGestureRecognizer:swipeRight];

    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(updatesOnLeftSwipe:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];

    UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(updatesOnDownSwipe:)];
    swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeDown];

    UISwipeGestureRecognizer *swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(updatesOnUpSwipe:)];
    swipeUp.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:swipeUp];

    // Do any additional setup after loading the view, typically from a nib.
}

// My method definitions
- (void)updatesOnRightSwipe:(UISwipeGestureRecognizer *)sender {
    
    // Game Over settings
    int j=0;
    while (![[[cellData objectAtIndex:j] text] isEqualToString:@""]) {
        gameOverFlag++;
        j++;
    }
    //NSLog(@"%d", gameOverFlag);
    
    for(int i=0;i<3;i++) {
        UILabel *cell1 = [cellData objectAtIndex:i];
        UILabel *cell2 = [cellData objectAtIndex:i+1];

        if(![[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i+1] text] isEqualToString:@"" ])
        {
            cell2.text = [[cellData objectAtIndex:i] text];
            [cell1 setText:@""];

        }
       else if((cell1.text.intValue != 0) && cell1.text.intValue == cell2.text.intValue) {
            cell2.text = [NSString stringWithFormat:@"%d", cell1.text.intValue + cell2.text.intValue];
            [cell1 setText:@""];
            score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell2.text.intValue];
        }
       else if([[[cellData objectAtIndex:i] text] isEqualToString:@""] && ![[[cellData objectAtIndex:i+1] text] isEqualToString:@"" ])
       {
           
       }
       else {
           if(i < 2) {
               UILabel *cell3 = [cellData objectAtIndex:i+2];
               if([[[cellData objectAtIndex:i+2] text] isEqualToString:@"" ]) {
                   cell3.text = [[cellData objectAtIndex:i+1] text];
                   cell2.text = [[cellData objectAtIndex:i] text];
                   [cell1 setText:@""];
               }
               else if((cell3.text.intValue != 0) && cell2.text.intValue == cell3.text.intValue) {
                   cell3.text = [NSString stringWithFormat:@"%d", cell2.text.intValue + cell3.text.intValue];
                   cell2.text = [[cellData objectAtIndex:i] text];
                   [cell1 setText:@""];
                   score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell3.text.intValue];
               }
               else if((cell3.text.intValue != 0) && cell2.text.intValue != cell3.text.intValue) {
                   flag = 1;
               }
           }
       }
        
     /*   if(flag == 1){
           // //NSLog(@"Flag set");
            UILabel *label4 = [cellData objectAtIndex:1];
            label4.text = [[cellData objectAtIndex:0] text];
            UILabel *initialLabel = [cellData objectAtIndex:0];
            [initialLabel setText:@""];
        }*/
    }
    for(int i=4;i<7;i++) {
        UILabel *cell1 = [cellData objectAtIndex:i];
        UILabel *cell2 = [cellData objectAtIndex:i+1];
        
        if(![[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i+1] text] isEqualToString:@"" ])
        {
            cell2.text = [[cellData objectAtIndex:i] text];
            [cell1 setText:@""];
            
        }
        else if((cell1.text.intValue != 0) && cell1.text.intValue == cell2.text.intValue) {
            cell2.text = [NSString stringWithFormat:@"%d", cell1.text.intValue + cell2.text.intValue];
            [cell1 setText:@""];
            score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell2.text.intValue];
        }
        else if([[[cellData objectAtIndex:i] text] isEqualToString:@""] && ![[[cellData objectAtIndex:i+1] text] isEqualToString:@"" ])
        {
            
        }
        else {
            if(i < 6) {
                UILabel *cell3 = [cellData objectAtIndex:i+2];
                if([[[cellData objectAtIndex:i+2] text] isEqualToString:@"" ]) {
                    cell3.text = [[cellData objectAtIndex:i+1] text];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue == cell3.text.intValue) {
                    cell3.text = [NSString stringWithFormat:@"%d", cell2.text.intValue + cell3.text.intValue];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                    score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell3.text.intValue];
                }
            }
        }
    }

    for(int i=8;i<11;i++) {
        UILabel *cell1 = [cellData objectAtIndex:i];
        UILabel *cell2 = [cellData objectAtIndex:i+1];
        
        if(![[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i+1] text] isEqualToString:@"" ])
        {
            cell2.text = [[cellData objectAtIndex:i] text];
            [cell1 setText:@""];
            
        }
        else if((cell1.text.intValue != 0) && cell1.text.intValue == cell2.text.intValue) {
                   cell2.text = [NSString stringWithFormat:@"%d", cell1.text.intValue + cell2.text.intValue];
                 [cell1 setText:@""];
                 score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell2.text.intValue];
        }
        else if([[[cellData objectAtIndex:i] text] isEqualToString:@""] && ![[[cellData objectAtIndex:i+1] text] isEqualToString:@"" ])
        {
            
        }
        else {
            if(i < 10) {
                UILabel *cell3 = [cellData objectAtIndex:i+2];
                if([[[cellData objectAtIndex:i+2] text] isEqualToString:@"" ]) {
                    cell3.text = [[cellData objectAtIndex:i+1] text];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue == cell3.text.intValue) {
                    cell3.text = [NSString stringWithFormat:@"%d", cell2.text.intValue + cell3.text.intValue];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                    score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell3.text.intValue];
                }
            }
        }

    }

    for(int i=12;i<15;i++) {
 ////NSLog(@"%d.", i);
        UILabel *cell1 = [cellData objectAtIndex:i];
        UILabel *cell2 = [cellData objectAtIndex:i+1];
        
        if(![[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i+1] text] isEqualToString:@"" ])
        {
         //   //NSLog(@"%d..", i);

            cell2.text = [[cellData objectAtIndex:i] text];
            [cell1 setText:@""];
         //   //NSLog(@"%d...", i);

        }
        else if((cell1.text.intValue != 0) && cell1.text.intValue == cell2.text.intValue) {
            cell2.text = [NSString stringWithFormat:@"%d", cell1.text.intValue + cell2.text.intValue];
            [cell1 setText:@""];
            score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell2.text.intValue];
        }
        else if([[[cellData objectAtIndex:i] text] isEqualToString:@""] && ![[[cellData objectAtIndex:i+1] text] isEqualToString:@"" ])
        {
            
        }
        else {
            if(i < 14) {
                UILabel *cell3 = [cellData objectAtIndex:i+2];
                if([[[cellData objectAtIndex:i+2] text] isEqualToString:@"" ]) {
                    cell3.text = [[cellData objectAtIndex:i+1] text];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue == cell3.text.intValue) {
                    cell3.text = [NSString stringWithFormat:@"%d", cell2.text.intValue + cell3.text.intValue];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                    score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell3.text.intValue];
                }
            }
        }

    }
    
    // Game Won settings
    for (int i=0; i<16; i++) {
        UILabel *currentCell = [cellData objectAtIndex:i];
        if([currentCell.text isEqual:@"2048"]) {
            //self.view.hidden = YES;
            gameWonView.hidden = NO;
            gameWonFlag = 1;
        }
    }
    
    if(gameOverFlag == 16) {
        for(int i=0; i<3; i++) {
            UILabel *cell1 = [cellData objectAtIndex:i];
            UILabel *cell2 = [cellData objectAtIndex:i+1];
            
            if(cell1.text.intValue != cell2.text.intValue) {
                stillEqualCells = 0;
            }
            else {
                stillEqualCells = 1;
            }
        }
        for(int i=4; i<7; i++) {
            UILabel *cell1 = [cellData objectAtIndex:i];
            UILabel *cell2 = [cellData objectAtIndex:i+1];
            
            if(cell1.text.intValue != cell2.text.intValue) {
                stillEqualCells = 0;
            }
            else {
                stillEqualCells = 1;
            }
        }
        for(int i=8; i<11; i++) {
            UILabel *cell1 = [cellData objectAtIndex:i];
            UILabel *cell2 = [cellData objectAtIndex:i+1];
            
            if(cell1.text.intValue != cell2.text.intValue) {
                stillEqualCells = 0;
            }
            else {
                stillEqualCells = 1;
            }
        }
        for(int i=12; i<15; i++) {
            UILabel *cell1 = [cellData objectAtIndex:i];
            UILabel *cell2 = [cellData objectAtIndex:i+1];
            
            if(cell1.text.intValue != cell2.text.intValue) {
                stillEqualCells = 0;
            }
            else {
                stillEqualCells = 1;
            }
        }
        
        if(stillEqualCells == 1)
        {
           // NSLog(@"dddd");
        }
    }
    else if(gameWonFlag != 16)
    {
    int rand3 = arc4random_uniform(16);
    while (![[[cellData objectAtIndex:rand3] text] isEqualToString:@""]) {
        rand3 = arc4random_uniform(16);
    }
  //  //NSLog(@"Rand %d", rand3);

    //Set new random cell
        UILabel *label3 = [cellData objectAtIndex:rand3];
        [label3 setText:@"2"];
    }
}


- (void)updatesOnLeftSwipe:(UISwipeGestureRecognizer *)sender {
    ////NSLog(@"ddd");
    /*   UILabel *label1 = [cellData objectAtIndex:0];
     UILabel *label2 = [cellData objectAtIndex:1];
     UILabel *label3 = [cellData objectAtIndex:2];
     UILabel *label4 = [cellData objectAtIndex:3];*/
    for(int i=3;i>0;i--) {
        UILabel *cell1 = [cellData objectAtIndex:i];
        UILabel *cell2 = [cellData objectAtIndex:i-1];
        
        if(![[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i-1] text] isEqualToString:@"" ])
        {
            cell2.text = [[cellData objectAtIndex:i] text];
            [cell1 setText:@""];
            
        }
        else if((cell1.text.intValue != 0) && cell1.text.intValue == cell2.text.intValue) {
            cell2.text = [NSString stringWithFormat:@"%d", cell1.text.intValue + cell2.text.intValue];
            [cell1 setText:@""];
            score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell2.text.intValue];
        }
        else if([[[cellData objectAtIndex:i] text] isEqualToString:@""] && ![[[cellData objectAtIndex:i-1] text] isEqualToString:@"" ])
        {
            
        }
        else {
            if(i > 1) {
                UILabel *cell3 = [cellData objectAtIndex:i-2];
                if([[[cellData objectAtIndex:i-2] text] isEqualToString:@"" ]) {
                    cell3.text = [[cellData objectAtIndex:i-1] text];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue == cell3.text.intValue) {
                    cell3.text = [NSString stringWithFormat:@"%d", cell2.text.intValue + cell3.text.intValue];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                    score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell3.text.intValue];
                }
            }
        }
        
    }
    for(int i=7;i>4;i--) {
        UILabel *cell1 = [cellData objectAtIndex:i];
        UILabel *cell2 = [cellData objectAtIndex:i-1];
        
        if(![[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i-1] text] isEqualToString:@"" ])
        {
            cell2.text = [[cellData objectAtIndex:i] text];
            [cell1 setText:@""];
            
        }
        else if((cell1.text.intValue != 0) && cell1.text.intValue == cell2.text.intValue) {
            cell2.text = [NSString stringWithFormat:@"%d", cell1.text.intValue + cell2.text.intValue];
            [cell1 setText:@""];
            score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell2.text.intValue];
        }
        else if([[[cellData objectAtIndex:i] text] isEqualToString:@""] && ![[[cellData objectAtIndex:i-1] text] isEqualToString:@"" ])
        {
            
        }
        else {
            if(i > 5) {
                UILabel *cell3 = [cellData objectAtIndex:i-2];
                if([[[cellData objectAtIndex:i-2] text] isEqualToString:@"" ]) {
                    cell3.text = [[cellData objectAtIndex:i-1] text];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue == cell3.text.intValue) {
                    cell3.text = [NSString stringWithFormat:@"%d", cell2.text.intValue + cell3.text.intValue];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                    score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell3.text.intValue];
                }
            }
        }

    }
    
    for(int i=11;i>8;i--) {
        UILabel *cell1 = [cellData objectAtIndex:i];
        UILabel *cell2 = [cellData objectAtIndex:i-1];
        
        if(![[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i-1] text] isEqualToString:@"" ])
        {
            cell2.text = [[cellData objectAtIndex:i] text];
            [cell1 setText:@""];
            
        }
        else if((cell1.text.intValue != 0) && cell1.text.intValue == cell2.text.intValue) {
            cell2.text = [NSString stringWithFormat:@"%d", cell1.text.intValue + cell2.text.intValue];
            [cell1 setText:@""];
            score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell2.text.intValue];
        }
        else if([[[cellData objectAtIndex:i] text] isEqualToString:@""] && ![[[cellData objectAtIndex:i-1] text] isEqualToString:@"" ])
        {
            
        }
        else {
            if(i > 9) {
                UILabel *cell3 = [cellData objectAtIndex:i-2];
                if([[[cellData objectAtIndex:i-2] text] isEqualToString:@"" ]) {
                    cell3.text = [[cellData objectAtIndex:i-1] text];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue == cell3.text.intValue) {
                    cell3.text = [NSString stringWithFormat:@"%d", cell2.text.intValue + cell3.text.intValue];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                    score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell3.text.intValue];
                }
            }
        }

    }
    
    for(int i=15;i>12;i--) {
        ////NSLog(@"%d.", i);
        UILabel *cell1 = [cellData objectAtIndex:i];
        UILabel *cell2 = [cellData objectAtIndex:i-1];
        
        if(![[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i-1] text] isEqualToString:@"" ])
        {
            //   //NSLog(@"%d..", i);
            
            cell2.text = [[cellData objectAtIndex:i] text];
            [cell1 setText:@""];
            //   //NSLog(@"%d...", i);
            
        }
        else if((cell1.text.intValue != 0) && cell1.text.intValue == cell2.text.intValue) {
            cell2.text = [NSString stringWithFormat:@"%d", cell1.text.intValue + cell2.text.intValue];
            [cell1 setText:@""];
            score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell2.text.intValue];
        }
        else if([[[cellData objectAtIndex:i] text] isEqualToString:@""] && ![[[cellData objectAtIndex:i-1] text] isEqualToString:@"" ])
        {
            
        }
        else {
            if(i > 13) {
                UILabel *cell3 = [cellData objectAtIndex:i-2];
                if([[[cellData objectAtIndex:i-2] text] isEqualToString:@"" ]) {
                    cell3.text = [[cellData objectAtIndex:i-1] text];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue == cell3.text.intValue) {
                    cell3.text = [NSString stringWithFormat:@"%d", cell2.text.intValue + cell3.text.intValue];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                    score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell3.text.intValue];
                }
            }
        }

    }
    
    // Game Won settings
    for (int i=0; i<16; i++) {
        UILabel *currentCell = [cellData objectAtIndex:i];
        if([currentCell.text isEqual:@"2048"]) {
            //self.view.hidden = YES;
            gameWonView.hidden = NO;
            gameWonFlag = 1;
        }
    }

    int rand3 = arc4random_uniform(16);
    while (![[[cellData objectAtIndex:rand3] text] isEqualToString:@""]) {
        rand3 = arc4random_uniform(16);
    }
    
    //NSLog(@"Rand %d", rand3);
    //Set new random cell
    UILabel *label3 = [cellData objectAtIndex:rand3];
    [label3 setText:@"2"];
}


- (void)updatesOnDownSwipe:(UISwipeGestureRecognizer *)sender {
    for(int i=0;i<12;i=i+4) {
        //NSLog(@"Down %d", i);
        UILabel *cell1 = [cellData objectAtIndex:i];
        UILabel *cell2 = [cellData objectAtIndex:i+4];
        
        if(![[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i+4] text] isEqualToString:@"" ])
        {
            cell2.text = [[cellData objectAtIndex:i] text];
            [cell1 setText:@""];
            
        }
        else if((cell1.text.intValue != 0) && cell1.text.intValue == cell2.text.intValue) {
            cell2.text = [NSString stringWithFormat:@"%d", cell1.text.intValue + cell2.text.intValue];
            [cell1 setText:@""];
            score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell2.text.intValue];
        }
        else if([[[cellData objectAtIndex:i] text] isEqualToString:@""] && ![[[cellData objectAtIndex:i+4] text] isEqualToString:@"" ])
        {
            
        }
        else if ([[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i+4] text] isEqualToString:@"" ])
        {
            
        }
        else {
            //NSLog(@"Down..");

            if(i < 7) {
                UILabel *cell3 = [cellData objectAtIndex:i+8];
                if([[[cellData objectAtIndex:i+8] text] isEqualToString:@"" ]) {
                    cell3.text = [[cellData objectAtIndex:i+4] text];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue == cell3.text.intValue) {
                    cell3.text = [NSString stringWithFormat:@"%d", cell2.text.intValue + cell3.text.intValue];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                    score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell2.text.intValue];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue != cell3.text.intValue) {
                    flag = 1;
                }
            }
        }
        /*
        if(flag == 1){
            // //NSLog(@"Flag set");
            UILabel *label4 = [cellData objectAtIndex:1];
            label4.text = [[cellData objectAtIndex:0] text];
            UILabel *initialLabel = [cellData objectAtIndex:0];
            [initialLabel setText:@""];
        }*/
    }
    for(int i=1;i<12;i=i+4) {
        //NSLog(@"Down %d", i);
        UILabel *cell1 = [cellData objectAtIndex:i];
        UILabel *cell2 = [cellData objectAtIndex:i+4];
        
        if(![[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i+4] text] isEqualToString:@"" ])
        {
            cell2.text = [[cellData objectAtIndex:i] text];
            [cell1 setText:@""];
            
        }
        else if((cell1.text.intValue != 0) && cell1.text.intValue == cell2.text.intValue) {
            cell2.text = [NSString stringWithFormat:@"%d", cell1.text.intValue + cell2.text.intValue];
            [cell1 setText:@""];
            score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell2.text.intValue];
        }
        else if([[[cellData objectAtIndex:i] text] isEqualToString:@""] && ![[[cellData objectAtIndex:i+4] text] isEqualToString:@"" ])
        {
            
        }
        else if ([[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i+4] text] isEqualToString:@"" ])
        {
            
        }
        else {
            //NSLog(@"Down..");
            
            if(i < 7) {
                UILabel *cell3 = [cellData objectAtIndex:i+8];
                if([[[cellData objectAtIndex:i+8] text] isEqualToString:@"" ]) {
                    cell3.text = [[cellData objectAtIndex:i+4] text];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue == cell3.text.intValue) {
                    cell3.text = [NSString stringWithFormat:@"%d", cell2.text.intValue + cell3.text.intValue];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                    score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell3.text.intValue];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue != cell3.text.intValue) {
                    flag = 1;
                }
            }
        }
        /*
         if(flag == 1){
         // //NSLog(@"Flag set");
         UILabel *label4 = [cellData objectAtIndex:1];
         label4.text = [[cellData objectAtIndex:0] text];
         UILabel *initialLabel = [cellData objectAtIndex:0];
         [initialLabel setText:@""];
         }*/
    }
    for(int i=2;i<12;i=i+4) {
        //NSLog(@"Down %d", i);
        UILabel *cell1 = [cellData objectAtIndex:i];
        UILabel *cell2 = [cellData objectAtIndex:i+4];
        
        if(![[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i+4] text] isEqualToString:@"" ])
        {
            cell2.text = [[cellData objectAtIndex:i] text];
            [cell1 setText:@""];
            
        }
        else if((cell1.text.intValue != 0) && cell1.text.intValue == cell2.text.intValue) {
            cell2.text = [NSString stringWithFormat:@"%d", cell1.text.intValue + cell2.text.intValue];
            [cell1 setText:@""];
            score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell2.text.intValue];
        }
        else if([[[cellData objectAtIndex:i] text] isEqualToString:@""] && ![[[cellData objectAtIndex:i+4] text] isEqualToString:@"" ])
        {
            
        }
        else if ([[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i+4] text] isEqualToString:@"" ])
        {
            
        }
        else {
            //NSLog(@"Down..");
            
            if(i < 7) {
                UILabel *cell3 = [cellData objectAtIndex:i+8];
                if([[[cellData objectAtIndex:i+8] text] isEqualToString:@"" ]) {
                    cell3.text = [[cellData objectAtIndex:i+4] text];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue == cell3.text.intValue) {
                    cell3.text = [NSString stringWithFormat:@"%d", cell2.text.intValue + cell3.text.intValue];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                    score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell3.text.intValue];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue != cell3.text.intValue) {
                    flag = 1;
                }
            }
        }
        /*
         if(flag == 1){
         // //NSLog(@"Flag set");
         UILabel *label4 = [cellData objectAtIndex:1];
         label4.text = [[cellData objectAtIndex:0] text];
         UILabel *initialLabel = [cellData objectAtIndex:0];
         [initialLabel setText:@""];
         }*/
    }
    for(int i=3;i<12;i=i+4) {
        //NSLog(@"Down %d", i);
        UILabel *cell1 = [cellData objectAtIndex:i];
        UILabel *cell2 = [cellData objectAtIndex:i+4];
        
        if(![[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i+4] text] isEqualToString:@"" ])
        {
            cell2.text = [[cellData objectAtIndex:i] text];
            [cell1 setText:@""];
            
        }
        else if((cell1.text.intValue != 0) && cell1.text.intValue == cell2.text.intValue) {
            cell2.text = [NSString stringWithFormat:@"%d", cell1.text.intValue + cell2.text.intValue];
            [cell1 setText:@""];
            score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell2.text.intValue];
        }
        else if([[[cellData objectAtIndex:i] text] isEqualToString:@""] && ![[[cellData objectAtIndex:i+4] text] isEqualToString:@"" ])
        {
            
        }
        else if ([[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i+4] text] isEqualToString:@"" ])
        {
            
        }
        else {
            //NSLog(@"Down..");
            
            if(i < 9) {
                UILabel *cell3 = [cellData objectAtIndex:i+8];
                if([[[cellData objectAtIndex:i+8] text] isEqualToString:@"" ]) {
                    cell3.text = [[cellData objectAtIndex:i+4] text];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue == cell3.text.intValue) {
                    cell3.text = [NSString stringWithFormat:@"%d", cell2.text.intValue + cell3.text.intValue];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                    score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell3.text.intValue];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue != cell3.text.intValue) {
                    flag = 1;
                }
            }
        }
        /*
         if(flag == 1){
         // //NSLog(@"Flag set");
         UILabel *label4 = [cellData objectAtIndex:1];
         label4.text = [[cellData objectAtIndex:0] text];
         UILabel *initialLabel = [cellData objectAtIndex:0];
         [initialLabel setText:@""];
         }*/
    }
    
    // Game Won settings
    for (int i=0; i<16; i++) {
        UILabel *currentCell = [cellData objectAtIndex:i];
        if([currentCell.text isEqual:@"2048"]) {
            //self.view.hidden = YES;
            gameWonView.hidden = NO;
            gameWonFlag = 1;
        }
    }

    
    int rand3 = arc4random_uniform(16);
    while (![[[cellData objectAtIndex:rand3] text] isEqualToString:@""]) {
        rand3 = arc4random_uniform(16);
    }
    
    //NSLog(@"Rand %d", rand3);
    //Set new random cell
    UILabel *label3 = [cellData objectAtIndex:rand3];
    [label3 setText:@"2"];

}


- (void)updatesOnUpSwipe:(UISwipeGestureRecognizer *)sender {

    for(int i=12;i>0;i=i-4) {
        UILabel *cell1 = [cellData objectAtIndex:i];
        UILabel *cell2 = [cellData objectAtIndex:i-4];
        
        if(![[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i-4] text] isEqualToString:@"" ])
        {
            cell2.text = [[cellData objectAtIndex:i] text];
            [cell1 setText:@""];
            
        }
        else if((cell1.text.intValue != 0) && cell1.text.intValue == cell2.text.intValue) {
            cell2.text = [NSString stringWithFormat:@"%d", cell1.text.intValue + cell2.text.intValue];
            [cell1 setText:@""];
            score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell2.text.intValue];
        }
        else if([[[cellData objectAtIndex:i] text] isEqualToString:@""] && ![[[cellData objectAtIndex:i-4] text] isEqualToString:@"" ])
        {
            
        }
        else {
            if(i > 7) {
                UILabel *cell3 = [cellData objectAtIndex:i-8];
                if([[[cellData objectAtIndex:i-8] text] isEqualToString:@"" ]) {
                    cell3.text = [[cellData objectAtIndex:i-4] text];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue == cell3.text.intValue) {
                    cell3.text = [NSString stringWithFormat:@"%d", cell2.text.intValue + cell3.text.intValue];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                    score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell3.text.intValue];
                }
            }
        }
        
    }
    for(int i=13;i>1;i=i-4) {
        UILabel *cell1 = [cellData objectAtIndex:i];
        UILabel *cell2 = [cellData objectAtIndex:i-4];
        
        if(![[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i-4] text] isEqualToString:@"" ])
        {
            cell2.text = [[cellData objectAtIndex:i] text];
            [cell1 setText:@""];
            
        }
        else if((cell1.text.intValue != 0) && cell1.text.intValue == cell2.text.intValue) {
            cell2.text = [NSString stringWithFormat:@"%d", cell1.text.intValue + cell2.text.intValue];
            [cell1 setText:@""];
            score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell2.text.intValue];
        }
        else if([[[cellData objectAtIndex:i] text] isEqualToString:@""] && ![[[cellData objectAtIndex:i-4] text] isEqualToString:@"" ])
        {
            
        }
        else {
            if(i > 7) {
                UILabel *cell3 = [cellData objectAtIndex:i-8];
                if([[[cellData objectAtIndex:i-8] text] isEqualToString:@"" ]) {
                    cell3.text = [[cellData objectAtIndex:i-4] text];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue == cell3.text.intValue) {
                    cell3.text = [NSString stringWithFormat:@"%d", cell2.text.intValue + cell3.text.intValue];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                    score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell3.text.intValue];
                }
            }
        }
        
    }
    for(int i=14;i>2;i=i-4) {
        UILabel *cell1 = [cellData objectAtIndex:i];
        UILabel *cell2 = [cellData objectAtIndex:i-4];
        
        if(![[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i-4] text] isEqualToString:@"" ])
        {
            cell2.text = [[cellData objectAtIndex:i] text];
            [cell1 setText:@""];
            
        }
        else if((cell1.text.intValue != 0) && cell1.text.intValue == cell2.text.intValue) {
            cell2.text = [NSString stringWithFormat:@"%d", cell1.text.intValue + cell2.text.intValue];
            [cell1 setText:@""];
            score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell2.text.intValue];
        }
        else if([[[cellData objectAtIndex:i] text] isEqualToString:@""] && ![[[cellData objectAtIndex:i-4] text] isEqualToString:@"" ])
        {
            
        }
        else {
            if(i > 7) {
                UILabel *cell3 = [cellData objectAtIndex:i-8];
                if([[[cellData objectAtIndex:i-8] text] isEqualToString:@"" ]) {
                    cell3.text = [[cellData objectAtIndex:i-4] text];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue == cell3.text.intValue) {
                    cell3.text = [NSString stringWithFormat:@"%d", cell2.text.intValue + cell3.text.intValue];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                    score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell3.text.intValue];
                }
            }
        }
        
    }
    for(int i=15;i>3;i=i-4) {
        UILabel *cell1 = [cellData objectAtIndex:i];
        UILabel *cell2 = [cellData objectAtIndex:i-4];
        
        if(![[[cellData objectAtIndex:i] text] isEqualToString:@""] && [[[cellData objectAtIndex:i-4] text] isEqualToString:@"" ])
        {
            cell2.text = [[cellData objectAtIndex:i] text];
            [cell1 setText:@""];
            
        }
        else if((cell1.text.intValue != 0) && cell1.text.intValue == cell2.text.intValue) {
            cell2.text = [NSString stringWithFormat:@"%d", cell1.text.intValue + cell2.text.intValue];
            [cell1 setText:@""];
            score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell2.text.intValue];
        }
        else if([[[cellData objectAtIndex:i] text] isEqualToString:@""] && ![[[cellData objectAtIndex:i-4] text] isEqualToString:@"" ])
        {
            
        }
        else {
            if(i > 7) {
                UILabel *cell3 = [cellData objectAtIndex:i-8];
                if([[[cellData objectAtIndex:i-8] text] isEqualToString:@"" ]) {
                    cell3.text = [[cellData objectAtIndex:i-4] text];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                }
                else if((cell3.text.intValue != 0) && cell2.text.intValue == cell3.text.intValue) {
                    cell3.text = [NSString stringWithFormat:@"%d", cell2.text.intValue + cell3.text.intValue];
                    cell2.text = [[cellData objectAtIndex:i] text];
                    [cell1 setText:@""];
                    score.text =  [NSString stringWithFormat:@"%d", score.text.intValue + cell3.text.intValue];
                }
            }
        }
        
    }
    
    // Game Won settings
    for (int i=0; i<16; i++) {
        UILabel *currentCell = [cellData objectAtIndex:i];
        if([currentCell.text isEqual:@"2048"]) {
            //self.view.hidden = YES;
            gameWonView.hidden = NO;
            gameWonFlag = 1;
        }
    }

    int rand3 = arc4random_uniform(16);
    while (![[[cellData objectAtIndex:rand3] text] isEqualToString:@""]) {
        rand3 = arc4random_uniform(16);
    }
    //NSLog(@"Rand %d", rand3);
    
    //Set new random cell
    UILabel *label3 = [cellData objectAtIndex:rand3];
    [label3 setText:@"2"];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
