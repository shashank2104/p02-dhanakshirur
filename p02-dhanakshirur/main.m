//
//  main.m
//  p02-dhanakshirur
//
//  Created by Shashank Dhanakshirur on 2/11/16.
//  Copyright © 2016 sdhanak1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
