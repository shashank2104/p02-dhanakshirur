//
//  ViewController.h
//  p02-dhanakshirur
//
//  Created by Shashank Dhanakshirur on 2/9/16.
//  Copyright © 2016 sdhanak1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIView *gameWonView;

@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *cellData;

@property (nonatomic, strong) IBOutlet UILabel *score;

@end

