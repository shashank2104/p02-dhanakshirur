//
//  AppDelegate.h
//  p02-dhanakshirur
//
//  Created by Shashank Dhanakshirur on 2/11/16.
//  Copyright © 2016 sdhanak1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

